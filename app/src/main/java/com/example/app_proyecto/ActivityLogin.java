package com.example.app_proyecto;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;


public class ActivityLogin extends AppCompatActivity implements View.OnClickListener {
    public static final String user="names";
    private Button buttonRegistrar;
    private Button banAccede;
    private TextView TextEmail;
    private TextView TextPassword;

    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;

    private FirebaseUser firebaseUser;
    TextView forgot_password;

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser user = firebaseAuth.getCurrentUser();

        if (user!=null){
            if(!user.isEmailVerified()){
                Toast.makeText(ActivityLogin.this,"Correo electrónico no verificado" + user.getEmail(),Toast.LENGTH_LONG).show();
            }else {
                startActivity(new Intent(ActivityLogin.this,ActividadEditarPerfil.class));
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ingresar");


        buttonRegistrar=(Button)findViewById(R.id.buttonRegistrar);
        banAccede =(Button)findViewById(R.id.btnAccede);
        TextEmail=(TextView)findViewById(R.id.editTextUser);
        TextPassword=(TextView)findViewById(R.id.editTextCla);
        forgot_password = findViewById(R.id.forgot_password);

        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityLogin.this,ResetPasswordActivity.class));
            }
        });

        firebaseAuth=FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);

        buttonRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        ActivityLogin.this, ActividadRegistrar.class
                );
                startActivity(intent);
            }
        });

        banAccede.setOnClickListener(this);


        }

    @Override
    public void onClick(View view) {
        //Invocamos al método:
        loguearUsuario();
    }

    private void loguearUsuario() {
        final String email = TextEmail.getText().toString().trim();
        String password= TextPassword.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            Toast.makeText(this,"Se debe ingresar un email",Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(password)){
            Toast.makeText(this,"Se debe ingresar una contra",Toast.LENGTH_LONG).show();
            return;
        }

        progressDialog.setMessage("Realizando consulta");
        progressDialog.show();



        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success

                        if(task.isSuccessful()){
                            FirebaseUser user = firebaseAuth.getCurrentUser();

                            if (user!=null){
                                if(!user.isEmailVerified()){
                                    Toast.makeText(ActivityLogin.this,"Correo electrónico no verificado " + user.getEmail(),Toast.LENGTH_LONG).show();
                                }else {
                                    Toast.makeText(ActivityLogin.this,"Bienvenido: "+ TextEmail.getText(),Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(ActivityLogin.this,ActividadEditarPerfil.class);
                                    startActivity(intent);
                                }
                            }
                        }else{
                            if(task.getException() instanceof FirebaseAuthUserCollisionException){//si se presenta una colision
                                Toast.makeText(ActivityLogin.this,"Ese usuario ya existe",Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(ActivityLogin.this,"No existe usuario",Toast.LENGTH_LONG).show();
                            }
                        }

                        progressDialog.dismiss();
                    }
                });
    }

}
