package com.example.app_proyecto.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.app_proyecto.Adapter.UserAdapter;
import com.example.app_proyecto.Model.User;
import com.example.app_proyecto.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class UsersFragment extends Fragment {
    private RecyclerView recyclerView;

    private UserAdapter userAdapter;
    private List<User> mUsers;

    //EditText search_users;

    //private String min;
    //private String max;

    //EditText t1,t2;

    String min;
    String max;

    public UsersFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_users, container, false);

        if (getArguments() != null) {
            min = getArguments().getString("MENOR");
            max = getArguments().getString("MAYOR");
        }

        /*if (getArguments() != null){
            min = getArguments().getString("MENOR");
            max = getArguments().getString("MAYOR");
            Toast.makeText(getContext(),"Min: "+min+"Max: "+max,Toast.LENGTH_SHORT).show();
        }*/
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mUsers = new ArrayList<>();

        readUser(min,max);
        return view;
    }


    /*private void searchUsers(String s) {

        final FirebaseUser fuser = FirebaseAuth.getInstance().getCurrentUser();
        Query query = FirebaseDatabase.getInstance().getReference("Users").orderByChild("search")
                .startAt(s)
                .endAt(s+"\uf8ff");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mUsers.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    User user = snapshot.getValue(User.class);

                    assert user != null;
                    assert fuser != null;
                    if (!user.getId().equals(fuser.getUid())){
                        mUsers.add(user);
                    }
                }

                userAdapter = new UserAdapter(getContext(),mUsers,false);
                recyclerView.setAdapter(userAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }*/



    private void readUser(String min, String max) {

        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        //final DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");

        Query usuarios = FirebaseDatabase.getInstance().getReference().child("Users").orderByChild("edad")
                .startAt(min)
                .endAt(max);

        usuarios.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mUsers.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    User  user = snapshot.getValue(User.class);


                    assert user != null;
                    assert firebaseUser != null;

                    if (!user.getId().equals(firebaseUser.getUid())){
                        mUsers.add(user);
                    }

                }
                //}

                userAdapter = new UserAdapter (getContext(),mUsers,false);
                recyclerView.setAdapter(userAdapter);
            }
            //String minimo = "18";
            //String maximo = "24";
                    /*Query usuarios = FirebaseDatabase.getInstance().getReference().child("Users").orderByChild("edad")
                            .startAt(minimo)
                            .endAt(maximo)

                    usuarios.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            //if (search_users.getText().toString().equals("")){
                            mUsers.clear();
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                                User  user = snapshot.getValue(User.class);

                                assert user != null;
                                assert firebaseUser != null;

                                if (!user.getId().equals(firebaseUser.getUid())){
                                    mUsers.add(user);
                                }

                            }
                            //}

                            userAdapter = new UserAdapter (getContext(),mUsers,false);
                            recyclerView.setAdapter(userAdapter);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });*/

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
